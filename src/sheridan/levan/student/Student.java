package sheridan.levan.student;

/**
 * This class represents students in our application
 *
 * @author Paul Bonenfant
 */
public class Student {
   //changes to commit
    //second commit
    private String name;
    private String id;
    private String program;

    public Student(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
   public String getId(){
        return id;
    }

    /**
     * @return the program
     */
    public String getProgram() {
        return program;
    }

    /**
     * @param program the program to set
     */
    public void setProgram(String program) {
        this.program = program;
    }
    
}

